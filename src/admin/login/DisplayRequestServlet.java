package admin.login;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import contactus.ConnectionProvider;
import contactus.Message;

/**
 * Servlet implementation class DisplayRequestServlet
 */
@WebServlet("/admin/login/DisplayRequestServlet")
public class DisplayRequestServlet extends HttpServlet {//dashboard servlet
	static Connection connection = ConnectionProvider.getConnection();

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		ArrayList<Message> list = new ArrayList();
		String query = "select * from messages";
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				list.add(
						new Message(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getBoolean(5)));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute("list", list);
		System.out.println("calling displayjsp");
		response.sendRedirect("../../displayRequests.jsp");//dashboard
	}

}
