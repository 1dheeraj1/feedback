package admin.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import admin.Admin;
import admin.AdminDAO;

/**
 * Servlet implementation class AdminLoginServlet
 */
@WebServlet("/admin/login/AdminLoginServlet")
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Admin admin = new Admin(username, password);
		if (AdminDAO.isUser(admin)) {
			HttpSession session = request.getSession();
			session.setAttribute("username", username);
			System.out.println("valid admin");
			// response.sendRedirect("../../displayRequests.jsp");
			RequestDispatcher rd = request.getRequestDispatcher("DisplayRequestServlet");
			rd.forward(request, response);
		} else {
			System.out.println("invalid admin");
			response.sendRedirect("../../contactUs.jsp");
		}

	}

}
