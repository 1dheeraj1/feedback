package admin;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import contactus.ConnectionProvider;

/**
 * Servlet implementation class Toggle
 */
@WebServlet("/admin/Toggle")
public class Toggle extends HttpServlet {
	static Connection connection=ConnectionProvider.getConnection();
	public void service(HttpServletRequest req,HttpServletResponse res) throws IOException, ServletException
	{
		
		String sid=req.getParameter("getId");
		int id=Integer.parseInt(sid);
		Statement st=null;
		try {
			st = connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String q="select active from messages where id="+id;
		ResultSet rs=null;
		try {
			rs = st.executeQuery(q);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		boolean val=false;
		try {
			while(rs.next())
			{
				val=rs.getBoolean(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement st2=null;
		try {
			st2 = connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String q2="update messages set active ="+!val+" where id="+id;
		ResultSet rs2=null;
		try {
			st2.executeUpdate(q2);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		res.sendRedirect("login/DisplayRequestServlet");
	}
}
