package contactus;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class MessagesDAO {
	static Connection connection = ConnectionProvider.getConnection();

	static boolean insert(Message message) {
		String q = "insert into messages (name,email,comment) values(?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(q);
			preparedStatement.setString(1, message.name);
			preparedStatement.setString(2, message.email);
			preparedStatement.setString(3, message.comment);
			preparedStatement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
