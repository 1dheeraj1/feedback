package contactus;

public class Message {
	int id;
	String name;
	String email;
	String comment;
	boolean active;

	public Message(String name, String email, String comment) {
		this.name = name;
		this.email = email;
		this.comment = comment;
	}

	public Message(int id, String name, String email, String comment, boolean active) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.comment = comment;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
