package contactus;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ContactUsServlet
 contactus.ContactUsServlet.doPost(HttpServletRequest, HttpServletResponse).message
 */
@WebServlet("/contactus/ContactUsServlet")
public class ContactUsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("name");
		String email=request.getParameter("email");
		String comment=request.getParameter("comment");
		Message message = new Message(name, email, comment);
		if(MessagesDAO.insert(message))
		{
			System.out.println("inserted");
		}
		else
		{
			System.out.println("insert failed");
		}
		response.sendRedirect("../contactUs.jsp");
	}

}
