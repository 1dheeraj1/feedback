<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Admin Login</title>
<style type="text/css">
.hint {
	color: grey;
	font-size: 11px;
}

.container {
	width: 25.9%;
	text-align: center;
	display: flex;
	vertical-align: middle;
	justify-content: center;
	align-content: center;
	align-items: center;
	background-color: #00fa9a;
	height: 80%;
	margin-top: 5%;
	border-radius: 10px;
}

input {
	border-radius: 5px;
	height: 30px;
	border-color: black;
}

input, textarea::placeholder {
	text-align: center;
}

.outer {
	height: 100vh;
	width: 100%;
	background-color: #2f4f4f;
	display: flex;
	justify-content: center;
}

form {
	margin-top: 0px;
}

.heading {
	display: flex;
	justify-content: center;
	text-align: center;
	height: 1cm;
	width: 80%;
	position: relative;
	left: 15px;
	border-bottom-left-radius: 70px;
	border-bottom-right-radius: 0px;
	border-top-left-radius: 0px;
	border-top-right-radius: 70px;
	border-color: red;
	border-width: 5px;
	border-style: solid;
	padding: 15px;
	box-sizing: content-box;
	font-size: 30px;
}

a {
	text-decoration: none;
}
</style>
</head>
<body>
	<div class="outer">
		<div class="container">

			<form action="admin/login/AdminLoginServlet" method="post">
				<div class="heading">Admin Login</div>
				<hr>
				Userame<br> <input type="text" name="username"
					placeholder="Enter admin username"><br>
				<br> Password <br> <input type="password" name="password"
					placeholder="Enter admin password"> <br>
				<br> <input type="submit" value="Login"> <br>
				<br> <a href="contactUs.jsp">Wanna go to Contact Us page?</a>
			</form>

		</div>
	</div>
</body>
</html>