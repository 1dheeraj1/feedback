<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
System.out.println("finally");
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");//http 1.1    
response.setHeader("Pragma", "no-cache");//http 1.0
response.setHeader("Expires", "0");//proxies    
if (session.getAttribute("username") == null) {
	response.sendRedirect("contactUs.jsp");
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Requests</title>
<style>
table, th, td {
	border: 1px solid black;
}

th, td {
	height: 0.8cm;
	width: 2.5cm;
}

table {
	background-color: #00fa9a;
	width: 100%;
}

.container {
	display: flex;
	justify-content: center;
	align-items: center; vertical-align : middle;
	height: 100vh;
	width: 50%;
	background-color: #2f4f4f;
	vertical-align: middle;
}

body {
	background-color: black;
	display: flex;
	justify-content: center;
	align-items: center;
}

input[value="Active"]:hover {
	background-color: green;
	height: 0.8cm;
	width: 2.5cm;
}

input[value="Archive"]:hover {
	background-color: red;
	height: 0.8cm;
	width: 2.5cm;
}
</style>
</head>

<c:set var="archiveCount" value="0"></c:set>
<c:set var="activeCount" value="0"></c:set>

<c:forEach items="${list}" var="message">
	<c:if test="${ message.active}">
		<c:set var="activeCount" value="${activeCount+1 }"></c:set>
	</c:if>
	<c:if test="${ !message.active}">
		<c:set var="archiveCount" value="${archiveCount+1 }"></c:set>
	</c:if>

</c:forEach>


<body style="">
	<a href="contactUs.jsp">Log out</a>
	<div class="container">
		<form action="admin/Toggle">
			<table id="ActiveTable">
				<c:if test="${activeCount>0}">
					<div style="position: relative; left: 250px; bottom: 20px">Active
						Requests</div>
					<tr>
						<th>name</th>
						<th>email</th>
						<th>comment</th>
						<th>status</th>
					</tr>
				</c:if>
				<c:forEach items="${list }" var="message">
					<c:if test="${ message.active}">
						<tr>
							<td>${message.name}</td>
							<td>${message.email}</td>
							<td>${message.comment}</td>
							<input type="hidden" value="${ message.id}" name="getId">
							<td><input type="submit" value="Archive"
								name="${message.id}"></td>
						</tr>
					</c:if>
				</c:forEach>
			</table>
		</form>
	</div>
	<div class="container">
		<form action="admin/Toggle">
			<table id="ArchiveTable">
				<c:if test="${archiveCount>0}">
					<div style="position: relative; left: 250px; bottom: 20px">Archive
						Requests</div>
					<tr>
						<th>name</th>
						<th>email</th>
						<th>comment</th>
						<th>status</th>
					</tr>
				</c:if>
				<c:forEach items="${list }" var="message">
					<c:if test="${ !message.active}">
						<tr>
							<td>${message.name}</td>
							<td>${message.email}</td>
							<td>${message.comment}</td>
							<input type="hidden" value="${ message.id}" name="getId">
							<td><input type="submit" value="Active" name="${message.id}"></td>
						</tr>
					</c:if>
				</c:forEach>
			</table>
		</form>
	</div>
</body>
</html>