<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Contact Us</title>
<style type="text/css">
span {
	color: red;
}

.hint {
	color: grey;
	font-size: 11px;
}

.container {
	text-align: center;
	display: flex;
	vertical-align: middle;
	justify-content: center;
	align-content: center;
	align-items: center;
	background-color: #00fa9a;
	height: 80%;
	margin-top: 5%;
	border-radius: 10px;
}

input {
	border-radius: 5px;
	height: 30px;
	border-color: black;
}

textarea {
	border-radius: 5px;
	border-color: black;
	margin-right: 10px;
	margin-left: 10px;
}

input, textarea::placeholder {
	text-align: center;
}

.outer {
	height: 100vh;
	width: 100%;
	background-color: #2f4f4f;
	display: flex;
	justify-content: center;
}

form {
	margin-top: 0px;
}

.heading {
	display: flex;
	justify-content: center;
	text-align: center;
	height: 1cm;
	width: 80%;
	position: relative;
	left: 15px;
	border-bottom-left-radius: 70px;
	border-bottom-right-radius: 0px;
	border-top-left-radius: 0px;
	border-top-right-radius: 70px;
	border-color: red;
	border-width: 5px;
	border-style: solid;
	padding: 15px;
	box-sizing: content-box;
	font-size: 30px;
}
</style>
</head>
<body>
<% 
session.removeAttribute("username");
session.invalidate();
System.out.println("invalidated");
%>
<div class="outer">
	<div class="container">
		
		<form action="contactus/ContactUsServlet" method="post">
			<div class="heading"> Contact Us</div>
			<hr>
			Full Name <span>*</span><br>
			<input type="text" name="name" placeholder="Enter your full name" ><br><br>
			E-mail <span>*</span><br>
			<input type="text" name="email" placeholder=	"Enter your E-mail">
			<div class="hint">example@example.com</div><br>
			Message <span>*</span><br>
			<textarea name="comment" cols="40" rows="5" placeholder="Enter your message"></textarea><br><br>
			<input type="submit" value="SUBMIT">
			<br><br>
			<a href="adminLogin.jsp">Are u an Admin?</a>
		</form>
		
	</div>
</div>	
</body>
</html>